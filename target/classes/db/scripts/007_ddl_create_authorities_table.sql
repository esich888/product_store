create table if not exists authorities (
  id serial primary key,
  authority text not null
);