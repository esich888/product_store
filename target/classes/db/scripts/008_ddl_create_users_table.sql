create table if not exists users (
  id serial primary key,
  username varchar not null,
  password varchar not null,
  enabled boolean default true not null,
  authority_id int references authorities(id)
);