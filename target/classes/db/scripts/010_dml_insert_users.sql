insert into users (username, enabled, password, authority_id)
values ('root', true, '$2a$10$qd/kPk80u0i9KyG7fiqYv.d6rEQYJ.klHaZBdubzaR.P4yRwupXDq',
(select id from authorities where authority = 'ROLE_ADMIN'));