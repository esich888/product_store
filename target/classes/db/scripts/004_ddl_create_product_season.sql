create table if not exists product_type (
  id serial primary key,
  product_id int not null references product(id),
  season_id int not null references season(id)
);