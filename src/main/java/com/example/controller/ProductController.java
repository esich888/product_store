package com.example.controller;

import com.example.entity.Product;
import com.example.service.ProductService;
import com.example.service.ProductTypeService;
import com.example.service.SeasonService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final ProductTypeService productTypeService;
    private final SeasonService seasonService;

    @GetMapping("/allProducts")
    public String getPageAllProducts(Model model) {
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        model.addAttribute("products", productService.findAll());
        return "products/listProduct";
    }

    @GetMapping("/create")
    public String getPageFormCreate(Model model) {
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        model.addAttribute("types", productTypeService.findAll());
        model.addAttribute("seasons", seasonService.findAll());
        return "products/formCreate";
    }

    @PostMapping("/create")
    public String createProduct(@ModelAttribute Product product) {
        productService.save(product);
        return "redirect:/allProducts";
    }

    @GetMapping("/update/{id}")
    public String updateProductById(@PathVariable int id, Model model) {
        Optional<Product> findProduct = productService.findById(id);
        if (findProduct.isEmpty()) {
            model.addAttribute("message", "данного товара не существует");
            return "errors/error";
        }
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        model.addAttribute("product", findProduct.get());
        model.addAttribute("types", productTypeService.findAll());
        model.addAttribute("seasons", seasonService.findAll());
        return "products/formUpdate";
    }

    @PostMapping("/update")
    public String updateProduct(@ModelAttribute Product product, Model model) {
        var isUpdate = productService.update(product);
        if (!isUpdate) {
            model.addAttribute("message", "Не удалось обновить данные");
            return "errors/error";
        }
        return "redirect:/allProducts";
    }

    @PostMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id) {
        productService.deleteById(id);
        return "redirect:/allProducts";
    }
}
