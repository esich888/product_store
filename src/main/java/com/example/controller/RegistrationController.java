package com.example.controller;

import com.example.entity.User;
import com.example.service.AuthorityService;
import com.example.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class RegistrationController {
    private final UserService userService;
    private final AuthorityService authorityService;

    @GetMapping("/formRegistration")
    public String regPage(Model model) {
        return "users/registration";
    }

    @PostMapping("/reg")
    public String regSave(@ModelAttribute User user, Model model) {
        var isCreate = userService.saveUser(user);
        if (!isCreate) {
            model.addAttribute("message", "такой пользователь уже существует");
            return "errors/registerError";
        }
        return "redirect:/formLogin";
    }
}
