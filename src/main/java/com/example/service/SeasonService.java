package com.example.service;

import com.example.entity.Season;
import com.example.repository.SeasonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class SeasonService {
    private final SeasonRepository repository;

    public List<Season> findAll() {
        return repository.findAll();
    }

    public Set<Season> findById(List<Integer> ids) {
        Set<Season> seasons = new HashSet<>();
        for (Integer rId : ids) {
            Optional<Season> season = repository.findById(rId);
            season.ifPresent(seasons::add);
        }
        return seasons;
    }
}
