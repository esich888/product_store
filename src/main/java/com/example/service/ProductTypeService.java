package com.example.service;

import com.example.entity.ProductType;
import com.example.repository.ProductTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class ProductTypeService {

    private final ProductTypeRepository productTypeRepository;

    public List<ProductType> findAll() {
        return productTypeRepository.findAll();
    }

    public Optional<ProductType> findById(int id) {
        return Optional.ofNullable(productTypeRepository.findById(id).orElseThrow(() ->
                new NoSuchElementException("Тип одежды с id = " + id + "не найден")));
    }
}
