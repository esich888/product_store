package com.example.service;

import com.example.entity.Product;
import com.example.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public Product save(Product product) {
      return  productRepository.save(product);
    }

    public List<Product> findAll() {
       return productRepository.findAll();
    }

    public Optional<Product> findById(int id) {
        return productRepository.findById(id);
    }

    public void deleteById(int id) {
        productRepository.deleteById(id);
    }

    public boolean update(Product product) {
        return productRepository.findById(product.getId())
                .map(prod -> {
                    prod.setName(product.getName());
                    prod.setDescription(product.getDescription());
                    prod.setPrice(product.getPrice());
                    return productRepository.save(prod) != null;
                })
                .orElseThrow(() -> new IllegalArgumentException("Product not found"));
    }
}
