package com.example.repository;

import com.example.entity.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> findByNameContaining(String name);

    List<Product> findByPriceGreaterThanEqual(double price);

    List<Product> findByNameContainingIgnoreCaseAndPriceGreaterThanEqual(String name, Double minPrice);
}
