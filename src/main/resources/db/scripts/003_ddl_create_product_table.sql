create table if not exists product (
  id serial primary key,
  name varchar not null,
  description text not null,
  price double precision not null,
  type_id int not null references type(id)
);