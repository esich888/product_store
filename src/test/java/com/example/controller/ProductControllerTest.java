package com.example.controller;

import org.mockito.Mockito;
import com.example.MainApp;
import com.example.entity.Product;
import com.example.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@SpringBootTest(classes = MainApp.class)
@AutoConfigureMockMvc
class ProductControllerTest {

    @MockBean
    private ProductService productService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser
    void whenGetPageAllAccidents() throws Exception {
        this.mockMvc.perform(get("/allProducts"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("products/listProduct"));

    }

    @Test
    @WithMockUser
    void whenGetPageFormCreate() throws Exception {
        this.mockMvc.perform(get("/create"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("products/formCreate"));
    }

    @Test
    @WithMockUser
    void wheGetPageFormUpdateByAccidentId() throws Exception {
        String id = "1";
        Mockito.when(productService.findById(1)).thenReturn(Optional.of(new Product()));
        this.mockMvc.perform(get("/update/{id}", id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("products/formUpdate"));
    }
}